<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nombre');
            $table->string('apellidos')->nullable();
            // unico por cliente, debería tener una relacion de uno a uno con la entidad cliente
            // eliminamos el unique(), para que pueda haber varios pedidos del mismo cliente
            $table->string('email');
            $table->unsignedInteger('telefono');
            $table->string('direccion');
            $table->date('fecha_entrega');
            $table->unsignedSmallInteger('franja_inicio');
            $table->unsignedSmallInteger('franja_fin');
            $table->timestamps();
            $table->integer('driver_id')->unsigned()->nullable();
            $table->foreign('driver_id')->references('id')->on('drivers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
