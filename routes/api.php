<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Use App\Pedido;
Use App\Driver;

Route::get('pedidos', 'PedidoController@index');

Route::get('pedidos/{id}', 'PedidoController@show');

Route::post('pedidos', 'PedidoController@create');

Route::get('drivers', 'DriverController@index');

Route::get('drivers/{id}', 'DriverController@show');

Route::post('drivers', 'DriverController@create');

Route::get('drivers/{id}/pedidos/{fecha}', 'DriverController@getPedidosByDay');
