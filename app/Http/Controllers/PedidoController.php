<?php

namespace App\Http\Controllers;

use App\Pedido;
use App\Driver;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PedidoController extends Controller
{
    //
    public function index()
    {
        return Pedido::all();
    }

    public function show($id)
    {
        return Pedido::find($id);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'nombre' => 'required',
            'telefono' => 'required|numeric',
            'direccion' => 'required|string',
            'fecha_entrega' => 'required|date|after:start_date',
            'franja_inicio' => 'required|numeric|between:0,24',
            'franja_fin' => [
                'required',
                'numeric',
                'between:0,24',
                function($atribute, $value, $fail) use ($request) {
                    $fin = $value - $request->franja_inicio;
                    if ($fin < 1 || $fin > 8) {
                       $fail($atribute. 'check limit for franja_inicio and franja_fin, must be a difference of 8 hours max or 1 hour min.');
                    }
                }
            ]
        ]);
        
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $request->offsetSet('driver_id', $this->randomDriver());
        $pedido = Pedido::create($request->all());
        return response()->json($pedido, 201);
    }

    /**
     * retrieve id of driver selected as random
     *
     * @return integer|null
     */
    private function randomDriver(): ?int
    {
        $drivers = Driver::all();
        if (!$drivers->isEmpty()) {
            $randomDriver = rand(0, count($drivers)-1);
            return $driver = $drivers[$randomDriver]->id;
        }
        return null;
    }
}
