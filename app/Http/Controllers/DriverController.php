<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Pedido;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    public function index()
    {
        return Driver::all();
    }

    public function show($id)
    {
        return Driver::find($id);
    }

    public function create(Request $request)
    {
        $driver = Driver::create($request->all());
        return response()->json($driver, 201);
    }

    public function getPedidosByDay($id, $fecha)
    {
        $time = strtotime($fecha);
        $date = date('Y-m-d', $time);
        
        $pedido = Pedido::where(function($query) use ($id, $date){
            $query->where('driver_id', '=', $id)
                ->where('fecha_entrega', '=', $date);
        })->get();

        return response()->json($pedido, 200);
    }
}
