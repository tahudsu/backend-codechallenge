<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    /**
     * se le asignan pedidos de manera random, una vez incluidos.
     * debe poder, recoger los pedidos en el día,para ellos un servicio con 
     * Id del driver y la fecha, que retorne todos los pedidos asignados.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    public function pedidos()
    {
        return $this->hasMany('App\Pedido');
    }
}
