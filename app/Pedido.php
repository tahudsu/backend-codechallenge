<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    
    /**
     *Nombre y apellidos del cliente
     * Email (Único por cliente)
     * Teléfono
     * Dirección de entrega (solo puede existir una por pedido)
     * Fecha de entrega
     * Franja de hora seleccionada para la entrega (variable, pueden ser desde franjas de 1h hasta de 8h) 
     */
    protected $fillable = ['nombre',
        'apellidos',
        'email',
        'telefono',
        'direccion',
        'fecha_entrega',
        'franja_inicio',
        'franja_fin',
        'driver_id'
    ];

    public function driver()
    {
        return $this->belongsTo('App\Driver', 'foreign_key');
    }  
}
